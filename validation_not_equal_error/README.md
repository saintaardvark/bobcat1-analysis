# kaitaistruct.ValidationNotEqualError: /types/callsigns/seq/0: at pos 19: validation failed: not equal, expected 'W8PZS', but got '\x06'

To duplicate:

- Download
  https://saintaardvarkthecarpeted.com/random/polaris/44420-482-20200329T164008Z-all.csv.gz
  (source: export from db.satnogs.org of all BOBCAT-1 frames)

- unzip `46922-482-20210508T210713Z-all.csv.gz` in the root of this
  repo
  
- run `make import-historical`

Full traceback:

```
$ make import-historical
source ~/dev/src/polaris/.venv/bin/activate && \
polaris fetch \
	--import_file `pwd`/46922-482-20210508T210713Z-all.csv \
	--start_date 2020-12-01 \
	--end_date 2021-04-24 \
	--skip_normalizer \
	BOBCAT-1 \
	./bobcat1-normalized_frames-historical.json
2021-05-08 14:21:48,233 - polaris.fetch.data_fetch_decoder - INFO - Satellite: id=46922 name=BOBCAT-1 decoder=bobcat1
2021-05-08 14:21:48,234 - polaris.fetch.data_fetch_decoder - INFO - selected decoder=bobcat1
2021-05-08 14:21:48,234 - polaris.fetch.fetch_import_telemetry - INFO - Starting decoding and merging of the new frames
Traceback (most recent call last):
  File "/home/aardvark/dev/src/polaris/.venv/bin/decode_multiple", line 8, in <module>
    sys.exit(main())
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decode_multiple.py", line 61, in main
    print(decode_multiple(args.decoder_name, parms, args.v))
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decode_multiple.py", line 171, in decode_multiple
    parse_csv_file(dname, params, json_obj, verbose)
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decode_multiple.py", line 118, in parse_csv_file
    create_point(decode_frame.decode_frame(dname, bindata),
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decode_frame.py", line 19, in decode_frame
    frame = decoder_class.from_bytes(raw_frame)
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/kaitaistruct.py", line 43, in from_bytes
    return cls(KaitaiStream(BytesIO(buf)))
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decoder/bobcat1.py", line 121, in __init__
    self._read()
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decoder/bobcat1.py", line 130, in _read
    self.frame = Bobcat1.Bc1ExtraFrame(self._io, self, self._root)
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decoder/bobcat1.py", line 258, in __init__
    self._read()
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decoder/bobcat1.py", line 263, in _read
    self.extra_frame = Bobcat1.ExtraData(self._io, self, self._root)
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decoder/bobcat1.py", line 174, in __init__
    self._read()
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decoder/bobcat1.py", line 178, in _read
    self.callsigns = Bobcat1.Callsigns(self._io, self, self._root)
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decoder/bobcat1.py", line 316, in __init__
    self._read()
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/satnogsdecoders/decoder/bobcat1.py", line 321, in _read
    raise kaitaistruct.ValidationNotEqualError(u"W8PZS", self.callsign, self._io, u"/types/callsigns/seq/0")
kaitaistruct.ValidationNotEqualError: /types/callsigns/seq/0: at pos 19: validation failed: not equal, expected 'W8PZS', but got '\x06'
2021-05-08 14:22:13,387 - polaris.fetch.fetch_import_telemetry - ERROR - decode_multiple cmd error. You can choose to ignore it by passing --ignore_errors flag in cmd
Traceback (most recent call last):
  File "/home/aardvark/dev/src/polaris/.venv/bin/polaris", line 33, in <module>
    sys.exit(load_entry_point('polaris-ml', 'console_scripts', 'polaris')())
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/click/core.py", line 829, in __call__
    return self.main(*args, **kwargs)
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/click/core.py", line 782, in main
    rv = self.invoke(ctx)
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/click/core.py", line 1259, in invoke
    return _process_result(sub_ctx.command.invoke(sub_ctx))
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/click/core.py", line 1066, in invoke
    return ctx.invoke(self.callback, **ctx.params)
  File "/home/aardvark/dev/src/polaris/.venv/lib/python3.8/site-packages/click/core.py", line 610, in invoke
    return callback(*args, **kwargs)
  File "/home/aardvark/dev/src/polaris/polaris/polaris.py", line 101, in cli_fetch
    data_fetch_decode_normalize(
  File "/home/aardvark/dev/src/polaris/polaris/fetch/data_fetch_decoder.py", line 197, in data_fetch_decode_normalize
    normalized_telemetry = fetch_normalized_telemetry(satellite, start_date,
  File "/home/aardvark/dev/src/polaris/polaris/fetch/fetch_import_telemetry.py", line 390, in fetch_normalized_telemetry
    decoded_frames_file = data_merge_and_decode(satellite.decoder, cache_dir,
  File "/home/aardvark/dev/src/polaris/polaris/fetch/fetch_import_telemetry.py", line 251, in data_merge_and_decode
    raise DecodeMultipleFailed
polaris.fetch.fetch_import_telemetry.DecodeMultipleFailed
```
