#!/bin/bash

# bash_template.sh:  A Small but Useful(tm) utility to foo the right bar.
#
# Copyright (C) 2021 Hugh Brown
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

# Much help with parsing args from:
# http://rsalveti.wordpress.com/2007/04/03/bash-parsing-arguments-with-getopts/
# Much help with "Unofficial Bash strict mode" from:
# http://redsymbol.net/articles/unofficial-bash-strict-mode/

set -eu
set -o pipefail
# Not sure I want this on all the time...
# IFS=$'\n\t'


usage() {
    cat << EOF
$0: A Small but Useful(tm) utility to foo the right bar.

usage: $0 options

OPTIONS:

   -l [int]	Number of lines to split (default 100)
   -f [file]	File to split
   -v		Verbose.
   -h		This helpful message.
EOF
    exit 1
}

complain_and_die() {
    echo $*
    exit 2
}

LINES=100
INFILE=
VERBOSE=
while getopts "l:f:vh" OPTION ; do
     case $OPTION in
         l) LINES=$OPTARG ; echo setting LINES to $LINES ;;
         f) INFILE=$OPTARG ;;
         v) VERBOSE=1 ;;
         h) usage ;;
         ?) usage ;;
     esac
done
shift "$((OPTIND-1))"

# Check for mandatory args
if [ -z $INFILE ]; then
     usage
fi

split --lines $LINES -d $INFILE

for i in x* ; do
    echo -n "$i ..."
    decode_multiple \
	-v \
	--filename $i \
	--format csv \
	BOBCAT1 > ${i}.json 2>&1 \
	&& echo "PASS" \
	    || echo "FAIL"
done
