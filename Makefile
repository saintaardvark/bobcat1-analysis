#
# Customize these variables:
#
# Path to virtualenv activation file.
PVENV=~/dev/src/polaris/.venv/bin/activate

# These were obtained from
# https://db.satnogs.org/satellite/44878
#
# You'll need to log in and select the Data tab, then select the
# option to export all frames.

BOBCAT_NORMALIZED_RECENT=./bobcat1-normalized_frames-recent.json
BOBCAT_GRAPH_RECENT=./bobcat1-graph-recent.json

BOBCAT_NORMALIZED_HISTORICAL=./bobcat1-normalized_frames-historical.json
BOBCAT_GRAPH_HISTORICAL=./bobcat1-graph-historical.json

recent: fetch-recent learn-recent
historical: fetch-historical learn-historical

fetch-recent:
	source $(PVENV) && \
	polaris fetch \
		--start_date 2021-04-25 \
		--end_date 2021-05-05 \
		--skip_normalizer \
		BOBCAT-1 \
		$(BOBCAT_NORMALIZED_RECENT)

learn-recent:
	source $(PVENV) && \
		time polaris learn \
		--learn_config_file ./learn.json \
		-g $(BOBCAT_GRAPH_RECENT) \
		--force_cpu \
		$(BOBCAT_NORMALIZED_RECENT)

view-recent:
	source $(PVENV) && \
		polaris viz \
		 $(BOBCAT_GRAPH_RECENT) 

fetch-historical:
	source $(PVENV) && \
	polaris fetch \
		--start_date 2020-12-01 \
		--end_date 2021-04-24 \
		--skip_normalizer \
		BOBCAT-1 \
		$(BOBCAT_NORMALIZED_HISTORICAL)

import-historical:
	source $(PVENV) && \
	polaris fetch \
		--import_file `pwd`/46922-482-20210508T210713Z-all.csv \
		--start_date 2020-12-01 \
		--end_date 2021-04-24 \
		--skip_normalizer \
		BOBCAT-1 \
		$(BOBCAT_NORMALIZED_HISTORICAL)

learn-historical:
	source $(PVENV) && \
		time polaris learn \
		--learn_config_file ./learn.json \
		-g $(BOBCAT_GRAPH_HISTORICAL) \
		--force_cpu \
		$(BOBCAT_NORMALIZED_HISTORICAL)

view-historical:
	source $(PVENV) && \
		polaris viz \
		 $(BOBCAT_GRAPH_HISTORICAL)
