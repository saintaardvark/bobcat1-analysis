# BOBCAT-1 Analysis

An analysis of BOBCAT-1 data, divided into two time periods:

- April 25 through May 5, 2021 (called "recent" in the Makefile; a
  period of increased tumbling)
- December 1, 2020 through April 24, 2021 (called "historical" in the
  Makefile; a somewhat arbitrary set of dates meant to capture
  "nominal" behaviour)

See the `Background` section for a bit more context

# Usage

Edit the Makefile and set `PVENV` appropriately.

**NOTE:** I've run this analysis using [this
branch](https://gitlab.com/saintaardvark/polaris/-/tree/issue-169-handle-exceptions-fetching-spaceweather)
in my personal repo; see [this
issue](https://gitlab.com/librespacefoundation/polaris/polaris/-/issues/169)
for background.

Run:

```
make recent
make historical
```

Screenshot:

![Screenshot_2021-05-08 Polaris - tmp bobcat1-graph-recent json.png](./Screenshot_2021-05-08 Polaris - tmp bobcat1-graph-recent json.png)

# A note on the learning parameters

Currently, the learning parameters in use are in `learn.json`.  A
default `polaris learn` -- that is, *without* using `learn.json` --
returned a nearly-empty graph.

Keep in mind that the parameters in `learn.json` were taken from [this
MR from
deckbsd](https://gitlab.com/librespacefoundation/polaris/polaris/-/merge_requests/187/diffs#44e45ae749fcd11d6cf82bedd5c21b1945088de1)
and not adjusted at all -- there may well be serious problems with
these parameters.

The graph file that produced this screenshot (that is, the one that I
generated locally) is in the `my_results` directory of this repo.

# Background
From chat:

> oldbug: Recently bobcat-1 has had some tumbling excess! Would anyone be able to do an analysis on the recent telemetry, specifically for investigating suns storms exceeding the gyro reading increase... Acinonyx threw this on the alerting channel

> phasewrap: the cause of the tumbling is up for debate but the reason it's happening is because detumbling was deactivated
> we had a comms failure a couple weeks ago, and as a result our mission watchdog tripped and bobcat-1 went into safemode
> We'll have a new groundstation radio in like 12 weeks, and I'm also building a SDR based groundstation in the meantime
> So in other words - the tumbling is expected, but what is causing it to spin up I am not really sure, there could be solar effects there
> btw yesterday was Bobcat-1's 6 month anniversary in space, and it also hit 32000 beacons the same day!


# Problems decoding Bobcat1 frames

I've tripped over a couple of problems when going through this analysis.

## TypeError: Object of type bytes is not JSON serializablep

One, documented in the `find_bad_frames` folder, was an exception when
decoding more recent frames:

```
TypeError: Object of type bytes is not JSON serializable
```

This was fixed with [this
MR](https://gitlab.com/librespacefoundation/satnogs/satnogs-decoders/-/merge_requests/194)
from @deckbsd.  Run:

```
pip install --upgrade satnogs-decoders
```

to get version 1.21.1.

## kaitaistruct.ValidationNotEqualError: /types/callsigns/seq/0: at pos 19: validation failed: not equal, expected 'W8PZS', but got '\x06'

Another problem, documented in the `validation_not_equal_error`
folder, occurred after the above error was fixed.  This cropped up
when running the `make import-historical` target.
